﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDEVMAT_17183t_1st.Models
{
    public abstract class Movable
    {
        public Vector3 Position = new Vector3();

        public double red = 1, green = 1, blue = 1, alpha = 1;

        public abstract void Render(OpenGL gl);
       
    }
}
